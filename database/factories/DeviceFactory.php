<?php

namespace Database\Factories;

use App\Enums\DevicePlatform;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DeviceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => Str::random(20),
            'uuid' => $this->faker->uuid(),
            'platform' => $this->faker->randomElement(array_column(DevicePlatform::cases(), 'value')),
            'primary_device_id' => Str::random(50),
            'os_language' => $this->faker->languageCode,
            'app_language' => $this->faker->languageCode,
            'os_version' => $this->faker->semver(),
            'app_version' => $this->faker->semver(),
            'build_version' => $this->faker->semver(),
            'app_bundle_id' => $this->faker->text(50),
            'device_name' => $this->faker->word(),
            'device_brand' => $this->faker->word(),
            'device_model' => $this->faker->word(),
            'carrier_name' => $this->faker->word(),
            'fcm_token' => Str::random(163),
        ];
    }
}
