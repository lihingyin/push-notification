<?php

namespace Database\Seeders;

use App\Models\Device;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Device::factory()
            ->create([
                'id' => 'abcde12345fghij67890',
            ]);
        Device::factory()
            ->count(10)
            ->create();
    }
}
