<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->string('id');
            $table->integer('user_id')->nullable();
            $table->string('uuid',36);
            $table->string('platform',10)->nullable();
            $table->string('primary_device_id',50); // Android `androidId`, iOS first UUID stored in key chain
            $table->string('os_language',5)->nullable();
            $table->string('app_language',5)->nullable();
            $table->string('os_version',20)->nullable();
            $table->string('app_version',50)->nullable();
            $table->string('build_version',10)->nullable();
            $table->string('app_bundle_id',50)->nullable();
            $table->string('device_name')->nullable();
            $table->string('device_brand')->nullable();
            $table->string('device_model')->nullable();
            $table->string('carrier_name')->nullable();
            $table->string('fcm_token')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
            $table->index('user_id');
            $table->unique('uuid');
            $table->index('platform');
            $table->unique('primary_device_id');
            $table->index('os_language');
            $table->index('app_language');
            $table->index('os_version');
            $table->index('app_bundle_id');
            $table->index('app_version');
            $table->index('build_version');
            $table->index('device_brand');
            $table->index('device_model');
            $table->index('carrier_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
