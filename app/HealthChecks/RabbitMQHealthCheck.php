<?php

declare(strict_types=1);

namespace App\HealthChecks;

use App\Helper\RabbitMqHelper;
use Exception;
use UKFast\HealthCheck\HealthCheck;
use UKFast\HealthCheck\Status;

class RabbitMQHealthCheck extends HealthCheck
{
    protected $name = 'RabbitMQ';

    public function status(): Status
    {
        try {
            $rabbitMqHelper = new RabbitMqHelper();
            $connection = $rabbitMqHelper->getConnection();
            $connection->close();
        } catch (Exception $e) {
            return $this->problem('Failed to connect to RabbitMq', [
                'exception' => $this->exceptionContext($e),
            ]);
        }

        return $this->okay();
    }
}
