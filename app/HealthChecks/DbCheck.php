<?php

declare(strict_types=1);

namespace App\HealthChecks;

use Throwable;
use UKFast\HealthCheck\Checks\DatabaseHealthCheck;

class DbCheck extends DatabaseHealthCheck
{
    protected $name = 'database';

    protected function exceptionContext(Throwable $e)
    {
        return [
            'error' => $e->getMessage(),
            'class' => \get_class($e),
            'line'  => $e->getLine(),
            'file'  => $e->getFile(),
            //'trace' => explode("\n", $e->getTraceAsString()), // disable trace for security reasons that should not show login credentials
        ];
    }
}
