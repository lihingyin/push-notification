<?php

declare(strict_types=1);

namespace App\HealthChecks;

use UKFast\HealthCheck\HealthCheck;
use UKFast\HealthCheck\Status;

class VersionCheck extends HealthCheck
{
    protected $name = 'version';

    public function status(): Status
    {
        exec('pwd', $pwd);

        $path  = './vendor/git_last_commit_hash';
        $path2 = '../vendor/git_last_commit_hash';
        if (file_exists($path)) {
            $gitHash = file_get_contents($path);
        } elseif (file_exists($path2)) {
            $gitHash = file_get_contents($path2);
        } else {
            $gitHash = 'default_git_hash';
        }
        $buildAt  = './vendor/build_time.txt';
        $buildAt2 = '../vendor/build_time.txt';
        if (file_exists($buildAt)) {
            $buildAt = file_get_contents($buildAt);
        } elseif (file_exists($buildAt2)) {
            $buildAt = file_get_contents($buildAt2);
        } else {
            $buildAt = 'build_at';
        }

        return $this->okay([
            'pwd'       => implode('', $pwd),
            'gitHash'   => trim($gitHash),
            'buildAt'   => trim($buildAt)
        ]);
    }
}
