<?php

namespace App\Http\Middleware;

use App\Exceptions\CustomAuthenticationException;
use Closure;
use Illuminate\Http\Request;

class HttpBasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $baseAuthUserName = config('app.base_auth.username');
        $baseAuthPassword = config('app.base_auth.password');
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        if (
            empty($request->server('PHP_AUTH_USER')) ||
            empty($request->server('PHP_AUTH_PW')) ||
            $request->server('PHP_AUTH_USER') != $baseAuthUserName ||
            $request->server('PHP_AUTH_PW') != $baseAuthPassword
        ) {
            if ($request->acceptsHtml()) {
                header('HTTP/1.1 401 Authorization Required');
                header('WWW-Authenticate: Basic realm="Access denied"');
                exit;
            }
            if ($request->acceptsJson()) {
                throw new CustomAuthenticationException();
            }
        }

        return $next($request);
    }
}
