<?php

namespace App\Http\Controllers;

use App\Enums\PushNotificationType;
use App\Http\Requests\PushNotificationdDliverToDevicesRequest;
use App\Jobs\queueDeliverPushNotificationToDevice;
use App\Models\Device;
use Illuminate\Http\JsonResponse;

class PushNotificationController extends Controller
{
    /**
     * To deliver push notification to devices
     *
     * @param PushNotificationdDliverToDevicesRequest $request
     * @return JsonResponse
     */
    public static function deliverToDevices(PushNotificationdDliverToDevicesRequest $request): JsonResponse
    {
        $messagesInput = $request->validated()['messages'];
        $deviceIds = array_column($messagesInput, 'deviceId');
        $texts = array_column($messagesInput, 'text');
        $messages = array_combine($deviceIds, $texts);

        $fcmTokens = (new Device)
            ->whereNotNull('fcm_token')
            ->whereIn('id', $deviceIds)
            ->pluck('fcm_token', 'id');
        foreach ($fcmTokens as $deviceId => $fcmToken) {
            queueDeliverPushNotificationToDevice::dispatch(
                type: PushNotificationType::DEVICE->value,
                deviceId: $deviceId,
                identifier: $fcmToken,
                text: $messages[$deviceId],
            );
        }
        return response()->json([
            'result' => true,
        ]);
    }
}
