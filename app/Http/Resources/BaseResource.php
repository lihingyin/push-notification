<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class BaseResource extends JsonResource
{
    public static function covertKeyToCamlCase($originalArray = [])
    {
        $returnArray = [];
        foreach ($originalArray as $key => $value) {
            $returnArray[Str::camel($key)] = $value;
        }
        return $returnArray;
    }
}
