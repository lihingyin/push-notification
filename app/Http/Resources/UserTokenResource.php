<?php

namespace App\Http\Resources;


class UserTokenResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = $this->resource;
        $array = parent::toArray($request);
        $token = $user->createToken($request->input('username'), $request->input('uuid'))->plainTextToken;

        return [
            'result' => true,
            'token' => $token,
            'user' => [
                "id" => $array['id'],
                "name" => $array['name'],
                "username" => $array['username'],
            ],
        ];
    }
}
