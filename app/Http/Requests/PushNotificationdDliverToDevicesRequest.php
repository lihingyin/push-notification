<?php

namespace App\Http\Requests;

use App\Models\Device;
use Illuminate\Foundation\Http\FormRequest;

class PushNotificationdDliverToDevicesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'messages.*.deviceId' => ['required', 'string', 'exists:' . Device::class . ',id'],
            'messages.*.text' => ['required', 'string'],
        ];
    }
}
