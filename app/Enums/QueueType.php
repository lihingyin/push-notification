<?php

namespace App\Enums;

enum QueueType: string
{
    case PUSH_NOTIFICATION = 'notification.fcm';
    // Other Queue Names here
}
