<?php

namespace App\Enums;

enum ExchangeType: string
{
    case PUSH_NOTIFICATION = 'push-notification';
    // Other Exchanges here
}
