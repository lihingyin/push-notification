<?php

namespace App\Enums;

enum TopicType: string
{
    case PUSH_NOTIFICATION_DONE = 'notification.done';
    // Other Topics here
}
