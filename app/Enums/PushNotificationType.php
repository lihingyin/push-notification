<?php

namespace App\Enums;

enum PushNotificationType: string
{
    case DEVICE = 'device';
    case USER = 'user';
    case TOPIC = 'topic';
}
