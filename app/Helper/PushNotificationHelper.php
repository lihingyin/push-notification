<?php

namespace App\Helper;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Kreait\Firebase\Contract\Messaging;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Throwable;

class PushNotificationHelper
{
    public Messaging $messaging;

    public function __construct()
    {
        $factory = (new Factory)->withServiceAccount(env('GOOGLE_APPLICATION_CREDENTIALS'));
        $this->messaging = $factory->createMessaging();
    }

    /**
     * To send push notification to device
     *
     * @param string $deviceToken
     * @param string $body
     * @param string|null $title
     * @return array|boolean
     */
    public function sendToDevice(
        string $deviceToken,
        string $body,
        ?string $title = null,
    )
    {
        $title ??= __('pushNotification.title');
        $message = CloudMessage::withTarget('token', $deviceToken)
            ->withNotification(Notification::create($title, $body));
        try {
            if (config('app.push_notification_test')) {
                // By pass delivering push notification to device
                return true;
            }

            return $this->messaging->send($message);
        } catch (Throwable $e) {
            Bugsnag::notifyError('Error occurred when sending push notifications to devices', $e->getMessage());
            return false;
        }
    }
}
