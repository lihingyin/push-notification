<?php

namespace App\Helper;

use App\Enums\ExchangeType;
use App\Enums\PushNotificationType;
use App\Enums\QueueType;
use App\Enums\TopicType;
use App\Models\FcmJob;
use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMqHelper
{
    protected AMQPStreamConnection $connection;

    public function __construct()
    {
        // Get connection if RabbitMQ
        $this->connection = AMQPStreamConnection::create_connection([
            [
                'host' => config('rabbitmq.host'),
                'port' => config('rabbitmq.port'),
                'user' => config('rabbitmq.user'),
                'password' => config('rabbitmq.pass'),
                'vhost' => config('rabbitmq.vhost'),
            ],
        ]);
    }

    /**
     * @throws Exception
     */
    public function getConnection(): AMQPStreamConnection
    {
        return $this->connection;
    }

    /**
     * To store push notification queue
     *
     * @param string $type
     * @param string $deviceId
     * @param string $identifier
     * @param string $text
     * @return void
     * @throws Exception
     */
    public function storePushNotificationQueue(
        string $type,
        string $deviceId,
        string $identifier,
        string $text,
    ): void
    {
        $queueName = QueueType::PUSH_NOTIFICATION->value;
        $channel = $this->connection->channel();
        $channel->queue_declare($queueName, false, true, false, false);

        $message = [
            'type' => $type,
            'deviceId' => $deviceId,
            'identifier' => $identifier,
            'text' => $text,
        ];
        $amqpMessage = new AMQPMessage(
            json_encode($message), [
                'content_type' => 'application/json',
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
            ]
        );

        $channel->basic_publish($amqpMessage, '', $queueName);

        $channel->close();
        $this->connection->close();
    }

    /**
     * To consume push notification queue
     *
     * @return void
     * @throws Exception
     */
    public function consumePushNotificationQueue(): void
    {
        $queueName = QueueType::PUSH_NOTIFICATION->value;
        $channel = $this->connection->channel();
        $channel->queue_declare($queueName, false, true, false, false);
        $channel->basic_consume($queueName, '', false, false, false, false, function ($data) use ($channel) {
            $exchange = ExchangeType::PUSH_NOTIFICATION->value;
            $topicName = TopicType::PUSH_NOTIFICATION_DONE->value;
            $message = json_decode($data->getBody(), false);
            switch ($message->type) {
                case PushNotificationType::DEVICE->value:
                default:
                    $pushNotificationResponse = (new PushNotificationHelper)->sendToDevice(
                        deviceToken: $message->identifier,
                        body: $message->text,
                    );
                    if (false !== $pushNotificationResponse) {
                        FcmJob::create([
                            'identifier' => $message->identifier,
                            'deliver_at' => now(),
                        ]);

                        // Publish a message to the `notification.done` topic
                        $channel->exchange_declare($exchange, 'topic', false, false, false);
                        $amqpMessage = new AMQPMessage(json_encode([
                            'identifier' => $message->identifier,
                            'deliverAt' => now(),
                        ]));
                        $channel->basic_publish($amqpMessage, $exchange, $topicName);
                    }
                    break;
                case PushNotificationType::USER->value:
                    // Function for push notification to users
                    break;
                case PushNotificationType::TOPIC->value:
                    // Function for push notification to specific topic
                    break;
            }

            $data->ack();
        });

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $this->connection->close();
    }

    /**
     * To consume push notification done topic
     *
     * @return void
     * @throws Exception
     */
    public function consumePushNotificationDoneTopic(): void
    {
        $exchange = ExchangeType::PUSH_NOTIFICATION->value;
        $topicName = TopicType::PUSH_NOTIFICATION_DONE->value;
        $channel = $this->connection->channel();

        $channel->exchange_declare($exchange, 'topic', false, false, false);

        [$queueName, ,] = $channel->queue_declare('', false, false, true, false);

        $channel->queue_bind($queueName, $exchange, $topicName);
        $channel->basic_consume($queueName, '', false, true, false, false, function ($data) {
            // Do something here
            echo $data->getBody() . PHP_EOL;
        });

        while ($channel->is_open()) {
            $channel->wait();
        }

        $channel->close();
        $this->connection->close();
    }
}
