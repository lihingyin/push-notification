<?php

namespace App\Helper;

use Illuminate\Http\Request;

class SystemHelper
{
    /**
     * Return client IPv4
     *
     * @param Request|null $request
     * @return string|null
     */
    public static function clientIp(?Request $request = null): ?string
    {
        $newRequest = $request ?? request();
        $ips = $newRequest->header('x-forwarded-for');
        foreach (explode(',', $ips) as $ip) {
            $ip = trim($ip);
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                return $ip;
            }
        }

        return $newRequest->ip();
    }
}
