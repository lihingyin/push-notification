<?php

namespace App\Console\Commands;

use App\Helper\RabbitMqHelper;
use Exception;
use Illuminate\Console\Command;

class consumeRabbitmqQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:consume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume queues of RabbitMQ';

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $this->info('Queues Consumer Started');

        (new RabbitMqHelper)->consumePushNotificationQueue();
    }
}
