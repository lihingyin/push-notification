<?php

namespace App\Console\Commands;

use App\Helper\RabbitMqHelper;
use Exception;
use Illuminate\Console\Command;

class consumeRabbitmqDoneTopic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:consumeTopic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consume topic of RabbitMQ';

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $this->info('Topics Consumer Started');

        (new RabbitMqHelper)->consumePushNotificationDoneTopic();
    }
}
