<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FcmJob extends Model
{
    use HasFactory;

    protected $table = 'fcm_job';
    protected $fillable = ['identifier', 'deliver_at'];

    public function setCreatedAt($value)
    {
    }

    public function setUpdatedAt($value)
    {
    }
}
