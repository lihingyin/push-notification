<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (AccessDeniedHttpException $e, Request $request) {
            throw new CustomAccessDeniedHttpException();
        });

        $this->renderable(function (AuthenticationException $e, Request $request) {
            throw new CustomAuthenticationException();
        });

        $this->renderable(function (NotFoundHttpException $e, Request $request) {
            throw new CustomNotFoundHttpException();
        });

        $this->renderable(function (MethodNotAllowedHttpException $e, Request $request) {
            throw new CustomMethodNotAllowedHttpException();
        });

        $this->renderable(function (ValidationException $e, Request $request) {
            throw new CustomValidationException($e->errors());
        });
    }
}
