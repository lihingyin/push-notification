<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class CustomMethodNotAllowedHttpException extends Exception
{
    public int $httpStatusCode;
    public ?string $bodyCode;

    public function __construct()
    {
        $this->httpStatusCode = Response::HTTP_METHOD_NOT_ALLOWED;
        $this->bodyCode = config('error.network.method_not_allowed');
        $this->message = __(config('error.network.method_not_allowed'));
        parent::__construct($this->message);
    }

    public function render()
    {
        return response()->json([
            'result' => false,
            'error' => [
                'message' => $this->message,
                'code' => $this->bodyCode,
            ]
        ], $this->httpStatusCode);
    }
}
