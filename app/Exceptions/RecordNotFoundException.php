<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class RecordNotFoundException extends CustomException
{
    public function __construct()
    {
        $this->httpStatusCode = SymfonyResponse::HTTP_FORBIDDEN;
        $this->bodyCode = config('error.not_found.record');
        $this->message = __(config('error.not_found.record'));

        parent::__construct(
            httpStatusCode: $this->httpStatusCode,
            bodyCode: $this->bodyCode,
            message: $this->message,
        );
    }
}
