<?php

namespace App\Jobs;

use App\Helper\RabbitMqHelper;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class queueDeliverPushNotificationToDevice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        protected string $type,
        protected string $deviceId,
        protected string $identifier,
        protected string $text,
    )
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        (new RabbitMqHelper)->storePushNotificationQueue(
            type: $this->type,
            deviceId: $this->deviceId,
            identifier: $this->identifier,
            text: $this->text,
        );
    }
}
