# Push Notification 

## Requirement
- PHP 8.1+

## Project Setup guide
_Run the following commands using the command line_

### Setup .env file 
- Go to Project directory
  - run `cd {project-directory}`
  - _(Example of `{project-directory}`: `~/development/repo/push-notification`)_
- run `cp -a .env.example .env`
- **_If a valid private key and FCM token could be provided, please set `PUSH_NOTIFICATION_TEST` to be `false`_**
- update `BUGSNAG_API_KEY` for enable error monitoring if wanted
- set `ACCESS_LOG_IN_DATABASE_ENABLE` to be `true` for enable access log storing in database
- set `ACCESS_LOG_IN_STDOUT_ENABLE` to be `true` for enable access log storing in stdout

### Run Composer Install with Docker
- run `docker run -it --rm --name composer2 -d -t -v {project-directory}:/home/source-code composer:2.0.12 composer install -d /home/source-code --ignore-platform-reqs`
- _The docker container `composer2` would be terminated after Composer Install is finished._
- _Please move on to the next step after it is finished._

### Setup Docker
- run `cd {project-directory}/docker`
- run `docker-compose -p push-notification up`

### Generate application key & setup Database
- run `docker exec -it push-notification-app sh -c "php artisan key:generate;php artisan migrate --seed;"`

### Health Check cURL
   ```
   curl --location --request GET 'http://localhost/health'
   ```

### Testing Steps
1. Start RabbitMQ queue consumer (The service expected to create)
   - run `docker exec -it push-notification-app sh -c "php artisan rabbitmq:consume"`
2. Start RabbitMQ topic consumer (The message content would be showed if the FCM message is delivered. Put in the same project for testing easier.)
   - run `docker exec -it push-notification-app sh -c "php artisan rabbitmq:consumeTopic"`
3. Run cURL to deliver push notification to devices (Put in the same project for testing easier.)
   ```
   curl --location --request PUT 'http://localhost/api/notification/deliver/users' \
   --header 'Content-Type: application/x-www-form-urlencoded' \
   --data-urlencode 'messages[0][deviceId]=abcde12345fghij67890' \
   --data-urlencode 'messages[0][text]=Try to bypass the SAS interface, maybe it will reboot the back-end panel!'
   ```
