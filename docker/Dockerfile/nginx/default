server {
  listen      8080;
  listen      [::]:8080 ipv6only=on;
  server_name -;
  charset     utf-8;

  root /var/www/html/public;
  index index.php index.html index.htm;

  client_max_body_size 50000M;
  client_body_timeout 6000;

  # Hardening
  location = /favicon.ico {
    log_not_found off;
    access_log off;
  }

  location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
  }

  location ~ /\. {
    deny all;
    access_log off;
    log_not_found off;
  }

  location / {
    try_files $uri $uri/ /index.php?$query_string;
  }

  location ~ \.php$ {
    try_files $uri /index.php =404;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass 127.0.0.1:9000;
    fastcgi_index index.php;
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_param HTTP_X_FORWARDED_FOR $http_x_forwarded_for if_not_empty;
    fastcgi_param SERVER_NAME axisplay.com;

    # Buffer
    fastcgi_connect_timeout 600;
    fastcgi_send_timeout    600;
    fastcgi_read_timeout    600;
    fastcgi_buffer_size     256k;
    fastcgi_buffers         8 256k;

    fastcgi_busy_buffers_size    256k;
    fastcgi_temp_file_write_size 256k;
    fastcgi_ignore_client_abort  off;
  }

  location ~ /\.ht {
    deny all;
  }

  # Error Page
  error_page 404 /404.html;
    location = /40x.html {
  }

  error_page 500 502 503 504 /50x.html;
    location = /50x.html {
  }
  # END Error Page
}
