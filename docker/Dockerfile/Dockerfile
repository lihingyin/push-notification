# Docker Image Name : raymondlhy/push-notification
# Command to build Docker Image : docker build --platform=linux/arm64 -t raymondlhy/push-notification:php8.1-arm64 .
# Command to build Docker Image : docker build --platform=linux/amd64 -t raymondlhy/push-notification:php8.1-amd64 .
FROM php:8.1.4-fpm
USER root
RUN apt-get update

# Install and Configure Nginx
RUN apt-get -y install wget gnupg2 \
    && wget https://nginx.org/keys/nginx_signing.key \
    && apt-key add nginx_signing.key \
    && echo "deb https://nginx.org/packages/debian/ buster nginx" >> /etc/apt/sources.list \
    && echo "deb-src https://nginx.org/packages/debian/ buster nginx" >> /etc/apt/sources.list \
    && apt-get -y remove nginx-common \
    && apt-get update \
    && apt-get -y install nginx

COPY nginx/default /etc/nginx/sites-enabled/
COPY nginx/nginx.conf /etc/nginx/
RUN chmod 755 /etc/nginx/sites-enabled/default \
	&& chmod 755 /etc/nginx/nginx.conf \
    && chmod 777 /var/log/nginx -Rf \
    && chmod 777 /var/cache/nginx/ -Rf \
	&& chmod 777 /run

# Update Unix user/group of PHP FPM processes
COPY php-fpm/www.conf /usr/local/etc/php-fpm.d/

# Add php.ini config
COPY php/custom_config.ini /usr/local/etc/php/conf.d/

# pdo_mysql, bcmath: officail laravel requirement
# sockets: rabbitmq
RUN docker-php-ext-install pdo_mysql bcmath sockets

# Override Entrypoint
COPY docker-php-entrypoint /usr/local/bin/
RUN chmod 775 /usr/local/bin/docker-php-entrypoint

RUN apt-get clean
EXPOSE 8080

# Create 'user' account
RUN useradd --create-home -s /bin/bash user
USER user
