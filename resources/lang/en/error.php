<?php

declare(strict_types=1);

return [
    'general' => [
        'try_again' => 'Something went wrong. Please try again.',
        'invalid_input' => 'The given data was invalid.',
    ],

    'network' => [
        'method_not_allowed' => 'The method is not supported for this route.',
        'not_found' => 'Page not found.',
    ],

    'user' => [
        'unauthorized' => 'This action is unauthorized.',
        'unauthenticated' => 'Unauthenticated.',
    ],

    'not_found' => [
        'record' => 'Record not found.',
    ],

    'device' => [
        'fcm_not_registered' => 'Device not registered for FCM.',
    ],
];
