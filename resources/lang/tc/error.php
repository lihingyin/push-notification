<?php

declare(strict_types=1);

return [
    'general' => [
        'try_again' => '出問題了。 請再試一次。',
        'invalid_input' => '無效輸入消息。',
    ],

    'network' => [
        'method_not_allowed' => '此路由不支持該方法。',
        'not_found' => '找不到網頁。',
    ],

    'user' => [
        'unauthorized' => '此操作未經授權。',
        'unauthenticated' => '未經身份驗證。',
    ],

    'not_found' => [
        'record' => '記錄不存在。',
    ],

    'device' => [
        'fcm_not_registered' => '裝置尚沒有登記FCM。',
    ],
];
