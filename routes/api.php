<?php

use App\Http\Controllers\PushNotificationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['accept.json', 'after']], function () {
    Route::put('notification/deliver/users',[PushNotificationController::class, 'deliverToDevices'])->name('deliverPushNotificationToUsers');
});
